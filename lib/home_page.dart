import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:sample_notification/api/notification_api.dart';
import 'package:sample_notification/notification_service.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page Notification"),
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: Center(
          child: Column(
            children: [
              ElevatedButton(
                onPressed: () => log("message"),
                child: Text("BUtton here"),
              ),
              ElevatedButton(
                onPressed: () => log("message"),
                child: Text("BUtton here"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
