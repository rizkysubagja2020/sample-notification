import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationService {

  // static final NotificationService _notificationService =
  //     NotificationService._internal();

  // factory NotificationService() {
  //   return _notificationService;
  // }

  // NotificationService._internal();

  ///////////////////////////////////////////////////////////////////////

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  // initiate
  Future<void> init() async {
    // set icon for notif
    final AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');

    // init setting
    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      // iOS: ,
      // macOS: ,
      // linux: ,
    );

    // initiate plugin local notification
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);
  }

  // function selected
  Future selectNotification(String? payload) async {
    //Handle notification tapped logic here

    // specifics channel android
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      "channel id",
      "channer name",
      channelDescription: "channel description",
      importance: Importance.max,
      priority: Priority.max,     
    );

    // Contains notification details specific to each platform.
    const NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      // iOS: ,
      // macOS: ,
      // linux: ,
    );

    // show notif
    await flutterLocalNotificationsPlugin.show(
      1,
      "title",
      "body",
      platformChannelSpecifics,
      payload: "payload",
    );
  }
}
