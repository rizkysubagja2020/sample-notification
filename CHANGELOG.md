# ChangeLog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/id-ID/1.0.0/)

## [0.0.1] - 2021-12-22

### Added

- Add changelog
